import os
import numpy as np
from scipy.interpolate import interp1d
import scipy.constants as spc
import typhon.constants as tyc
from netCDF4 import Dataset

__all__ = [
    'density_of_molecules',
    'overhead_molecules',
    'add_layer_dim',
    'calculate_k2',
    'calculate_k3',
    'nox_reaction_rates',
    'hox_reaction_rates',
    'get_hox_data',
]


def density_of_molecules(p, T):
    """
    Parameters:
        p (ndarray): pressure levels [Pa]
        T (ndarray): temperature values [K]
    Returns:
        ndarray: density of molecules [number of molecules / m3]
    """
    return (tyc.avogadro * p) / (spc.gas_constant * T)


def overhead_molecules(gas, p, z, T):
    """
    Parameters:
        gas (ndarray): gas concentration [ppv] corresponding to levels p
        p (ndarray): pressure levels [Pa]
        z (ndarray): height values [m]
        T (ndarray): temperature values [K]
    Returns:
        ndarray: number of molecules per m2 in overhead column
    """
    # molecules / m2 of air in each layer
    molecules_air = density_of_molecules(p, T) * np.gradient(z)
    # overhead column in molecules / m2
    col = np.flipud(np.cumsum(np.flipud(gas * molecules_air)))

    return col


def add_layer_dim(data, numlevels=200):

    data = np.expand_dims(data, len(data.shape))
    data = np.repeat(data, numlevels, axis=len(data.shape)-1)
    return data


def calculate_k2(T):
    """Calculate temperature dependent reaction rate.
    Parameters:
        T(ndarray): temperature[K]
    Returns:
        ndarray: reaction rate [m6 / molecule2 / s]
    """
    return 6E-46 * (T / 300)**(-2.4)


def calculate_k3(T):
    """Calculate temperature dependent reaction rate.
    Parameters:
        T(ndarray): temperature[K]
    Returns:
        ndarray: reaction rate [m3 / molecule / s]
    """
    return 8E-18 * np.exp(-2060 / T)


def nox_reaction_rates(T):
    """Calculate temperature dependent reaction rates.

    Parameters:
        T (ndarray): temperature [K]
    Returns:
        ndarray: reaction rate [m3 / molecule / s]
        ndarray: reaction rate [m3 / molecule / s]
    """
    b3 = 5.1E-18 * np.exp(210 / T)
    b4 = 3.0E-18 * np.exp(-1500 / T)
    return b3, b4


def hox_reaction_rates(T):
    """
    Parameters:
        T (int/float/ndarray): temperature [K]
    Returns:
        float/ndarray: reaction rate [m3 / molecule / s]
        float/ndarray: reaction rate [m3 / molecule / s]
        float/ndarray: reaction rate [m3 / molecule / s]
        float/ndarray: reaction rate [m3 / molecule / s]
        float/ndarray: reaction rate [m6 / molecule2 / s]
    """
    a5 = 1.8E-17 * np.exp(180 / T)  # m3 / molecule / s
    a7 = 3.0E-17 * np.exp(200 / T)  # m3 / molecule / s

    a6 = 1.7e-18 * np.exp(-940 / T)  # m3 / molecule / s
    a6b = 1e-20 * np.exp(-490 / T)  # m3 / molecule / s

    return a5, a7, a6, a6b,

#
# ec_20190731
#
# This is the version of the hox_data function that
# pulls from the haloe data. I wrote a version that
# pulls from some escimo data that I recently compiled.
#
#def get_hox_data(p):
#    """
#    Parameters:
#        p (ndarray): pressure levels
#    Returns:
#        ndarray: OH concentration [molecules / m3] at each pressure level
#        ndarray: HO2 concentration [molecules / m3] at each pressure level
#    """
#
#    ds = Dataset(
#        os.path.join(os.path.dirname(__file__),
#                     '../data/avg_twod_haloe_clams_jpl11.nc'))
#
#    oh_data = np.mean(ds['OH'][:, :, 7:11], axis=(0, -1))  # tropical mean
#    ho2_data = np.mean(ds['HO2'][:, :, 7:11], axis=(0, -1))  # 20N-20S
#
#    p_data = ds['press'][:]*100
#
#    oh = interp1d(p_data, oh_data, fill_value='extrapolate')(p)
#    ho2 = interp1d(p_data, ho2_data, fill_value='extrapolate')(p)

def get_hox_data(p):
    """
    Parameters:
        p (ndarray): pressure levels
    Returns:
        ndarray: OH concentration [molecules / m3] at each pressure level
        ndarray: HO2 concentration [molecules / m3] at each pressure level
    """

    ds = Dataset(
        os.path.join(os.path.dirname(__file__),
                     '../data/avg_twod_haloe_clams_jpl11.nc'))
    # tropical 20N-20S (indices 7:11) and diurnal mean of OH and HO2
    oh_data = np.mean(ds['OH'][:, :, 7:11], axis=(0, -1))
    ho2_data = np.mean(ds['HO2'][:, :, 7:11], axis=(0, -1))
    p_data = ds['press'][:]*100

    # interpolate onto our pressure levels
    diurnal_hox = interp1d(
        p_data, oh_data+ho2_data, fill_value='extrapolate')(p)

    # assume that there is no HOx during night (Fig 5.27 of Brasseur and
    # Solomon shows that the lifetime of HOx is ~ 20 minutes below ~ 55 km)
    # --> daytime mean is twice the diurnal mean
    hox = 2 * diurnal_hox

    return hox * 10**6
