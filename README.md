### Dear users,

This version of the model contains only the components necessary for ozone 
chemistry column calculations. 
If this model is used iteratively to calculate the ozone profile corresponding
to a specified temperature profile (either specified or itself calculated 
iteratively depending on the ozone profile), ozone transport should also be
included. This is especially important in the tropopause layer and lower
stratosphere.

Please contact me before
you use it as I would like to know who is using it and would also just be
interested in hearing what you're working on.

Peace,

Ed

edward.charlesworth.science@gmail.com


# Simotrostra

This model calculates changes in ozone due to chemistry in a column of the 
atmosphere. The only chemistry considered is that of pure oxygen,
NOx and HOx and only the most important reactions are considered.

### Ozone source

The odd oxygen (Ox = O + O* + O<sub>3</sub>) source term is based on the
following photolysis reactions.

&nbsp;&nbsp; J2: O<sub>2</sub> + hv(lambda<242nm) &rightarrow;
                 O + O

&nbsp;&nbsp; J2*: O<sub>2</sub> + hv(lambda<175nm) &rightarrow;
                  O + O*

These are the only two reactions that create odd oxygen, other reactions either
convert between one type of odd oxygen and another, or act to destroy odd
oxygen.    
We then make the assumption that Ox is approximately equal to O<sub>3</sub>
below the mesosphere, so a source of Ox is a source of O<sub>3</sub>.
This gives an ozone source term equal to 2 (J2 + J2*) \[O<sub>2</sub>].

### Odd oxygen partitioning

To calculate the odd oxygen tendencies, the model requires an ozone profile
as input, along with temperature, pressure and height profiles and a specified
zenith angle.
When running the model iteratively (see examples/iterate_ozone_profile.py),
we use the odd oxygen tendencies to update the ozone profile.

We do not use the tendencies to update the concentration of atomic oxygen
directly, but instead we calculate the concentration of atomic oxygen from that
of ozone.
This partitioning is based on the following reactions.

&nbsp;&nbsp; J3: O<sub>3</sub> + hv(lambda<1180nm) &rightarrow;
                 O<sub>2</sub> + O

&nbsp;&nbsp; J3*: O<sub>3</sub> + hv(lambda<411nm) &rightarrow;
                  O<sub>2</sub> + O*

&nbsp;&nbsp; k2: O<sub>2</sub> + O + M &rightarrow; O<sub>3</sub> + M

&nbsp;&nbsp; k4: O* + M &rightarrow; O + M

Because the chemical timescales are short, we can assume that O and O* are in a
steady state.

&nbsp;&nbsp; O* = (J3* &times; O<sub>3</sub>) / (k4 &times; M) 

&nbsp;&nbsp; O = (J3 &times; O<sub>3</sub> + k4 &times; O* &times; M) / 
                 (k2 &times; O<sub>2</sub> &times; M)

Combining these gives

&nbsp;&nbsp; O = (J3 + J3*) &times; O<sub>3</sub> / 
                 (k2 &times; O<sub>2</sub> &times; M)

At nighttime we assume that there is no atomic oxygen, as there are no photons
available for the photodissociation of O<sub>3</sub> and atomic oxygen quickly
undergoes recombination reactions.

### Photolysis Rates

Both the ozone source term and odd oxygen partitioning (which is used in
the Ox, NOx and HOx sink terms) involve photolysis reactions.
The two photolysis rate coefficients we are interested in are
J<sub>O<sub>3</sub></sub> = J3 + J3* and
J<sub>O<sub>2</sub></sub> = J2 + J2*.
The photolysis rate coefficients are calculated for each model level by 
multiplying the absorption cross-section by the amount of radiation reaching
that level and integrating in wavelength space.
For both J<sub>O<sub>2</sub></sub> and J<sub>O<sub>3</sub></sub>, we assume a
quantum efficiency of 1.

The calculation of J<sub>O<sub>3</sub></sub> is relatively simple, because we
can ignore the small dependence of the absorption cross-section on temperature
and pressure.
Then, we only need to take into account the wavelength dependence of the
absorption cross-section, as well as the flux of photons of each wavelength
through the atmosphere.
The flux of photons of a certain wavelength reaching a certain depth depends on
the incoming irradiance (which depends on zenith angle) and the transmissivity
of the atmosphere above.
Flux at wavelengths relevant for J<sub>O<sub>3</sub></sub> is only absorbed in
the photolysis reactions with ozone, so the transmissivity depends only on the
amount of ozone and the absorption cross-section.

For the calculation of J<sub>O<sub>2</sub></sub>, we consider two wavelength
ranges separately, the Schumann-Runge bands between 175 and 205 mm and the
Herzberg continuum between 205 and 245mm.
The contribution to J<sub>O<sub>2</sub></sub> from the Schumann-Runge bands we
will denote as J<sub>srb</sub> and that from the Herzberg continuum as
J<sub>hz</sub>.

To calculate J<sub>hz</sub>, we need to consider the pressure dependence of the
oxygen absorption cross-section, which is approximately linear (Yoshino et al. 
1988).
In addition, when calculating the flux of photons of a certain wavelength
reaching a certain depth in the atmosphere, we need to consider both absorption
due to the photolysis reactions with O<sub>2</sub> and absorption by ozone.

For J<sub>srb</sub>, the absorption cross-sections are highly dependent on
wavelength such that a very high spectral resolution would be required.
Instead, we use the parameterisation of Minschwaner et al.
 1993<sup id="minschwaner93">[1](#minschwaner)</sup>.
Wavelength information is not explicitly represented in this model, as it has 
been baked into the data. That data can be obtained by contacting Minschwaner.

The sources for data for the non-parameterized part are WMO Report No 16:
Atmospheric Ozone (1985) for wavelength and top-of-atmosphere flux data and 
JPL Evaluation 18 (Chemical Kinetics and Photochemical Data for Use in
Atmospheric Studies) for cross-section data and quantum yield data (the
quantum yield is always 1 for this, so actually it's not really a source of
data so much as a reference).

### Ox sink

Odd oxygen can be removed from the atmosphere by the reaction

&nbsp;&nbsp; k3: O<sub>3</sub> + O &rightarrow; 2 O<sub>2</sub>

There are also other reactions between odd oxygen species, but these are of
second order importance and are neglected here.

### NOx sink

The NOx family partitioning chemistry is based on:

&nbsp;&nbsp; b3: NO<sub>2</sub> + O  &rightarrow; NO  + O<sub>2</sub>

&nbsp;&nbsp; b4: NO + O<sub>3</sub> &rightarrow; NO<sub>2</sub> + O<sub>2</sub>

Assuming steady state, then 

&nbsp;&nbsp; NO/NO<sub>2</sub> = (b3 &times; O) / (b4 &times; O<sub>3</sub>)

Rates from JPL document 15-10, pages 1-70 and 1-71:

&nbsp;&nbsp; b3 = 5.1 &times; 10<sup>-12</sup> &times; exp(210/T)
    
&nbsp;&nbsp; b4 = 3.0 &times; 10<sup>-12</sup> &times; exp(-1500/T)

where the temperature, T, is given in Kelvin and the units of the rate
coefficients are cm<sup>3</sup>/molecule/s.

The loss of Ox due to NOx is:

&nbsp;&nbsp; sink_nox = b4 &times; NO &times; O<sub>3</sub> + 
                      b3 &times; NO<sub>2</sub> &times; O

where O, NO and NO<sub>2</sub> are all molecule densities.

We do not include the photolysis reaction of NO<sub>2</sub> to NO, which
produces odd oxygen.

&nbsp;&nbsp; JNO2:  NO<sub>2</sub> + hv(lambda<405nm) &rightarrow; NO + O

Excluding this means that NO levels are lower during daytime, 
but at high altitudes they're already dominant (NOx ~= NO) during daytime,
so this is really just an effect at lower altitudes where O concentrations are 
in any case low.

### HOx sink

The following reactions involving members of the HOx family are included,
both in the partitioning of HOx into OH and HO<sub>2</sub>
and for the destruction of odd oxygen.

&nbsp;&nbsp; a5: OH + O &rightarrow; O<sub>2</sub> + H

&nbsp;&nbsp; a7: HO<sub>2</sub> + O &rightarrow; O<sub>2</sub> + OH

&nbsp;&nbsp; a6: OH + O<sub>3</sub> &rightarrow; O<sub>2</sub> + HO<sub>2</sub>

&nbsp;&nbsp; a6b: HO<sub>2</sub> + O<sub>3</sub> &rightarrow; 2 O<sub>2</sub> + OH

The first two are important in the upper stratosphere, while the third and
forth are important in the middle and lower stratosphere respectively.

These reactions convert one member of the OH to HO<sub>2</sub> and vice versa,
but do not actually create or destroy HOx.
HOx is created via photolysis reactions, mainly from H<sub>2</sub>O.
The lifetime of HOx in the stratosphere is around 20 minutes
(Figure 5.27 of Brasseur and Solomon "Aeronomy of the Middle Atmosphere"),
so during nighttime there is very little HOx.
For simplicity, we do not include the photolysis reactions that create HOx,
but simply specify a HOx profile. At night, we assume there is no HOx and
during the day we take the mean daytime profile, calculated as twice the
diurnal profile of OH + HO<sub>2</sub>.

For the partitioning, we are making the assumption that OH and HO<sub>2</sub>
are in chemical equilibrium, which is reasonable as their lifetimes are short
(on the order of seconds).

The rate coefficients, a5, a7, a6 and a6b are from the JPL Evaluation 18.

Other reactions (e.g., including CO and NO) are neglected,
and we also neglect atomic hydrogen,
which destroys ozone and would affect the HOx partitioning
(HOx = OH + HO<sub>2</sub> + H).
H only becomes important in the upper stratosphere and mesosphere.

#### About naming conventions and units

"sigma" denotes cross-sections,
"flux" denotes radiative fluxes in photons per area and time, 
"hz" stands for "Herzberg" but really that means everything from 205-800 nm,
while "srb" stands for "Schumann-Runge Bands".
Most of the units are SI units (International System of units),
so that means kg, m, K, etc.
Wavelengths are in nanometers.

#### Footnote

<a id="minschwaner">1</a>[↩](#minschwaner93): This paper is absolutely the
clearest explanation for a Schumann-Runge band parameterization that I found
and is totally readable. 

Also, the last part of The Changing Stratosphere by 
McElroy, Salawitch, and Minschwaner, which is Ken Minschwaner's part, is a 
really interesting 1-D RCE work that examines the impacts of ozone on tropical
middle atmosphere temperatures. It's totally worth reading that section if 
you're working with this model.
