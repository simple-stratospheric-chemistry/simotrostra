from simotrostra import Simotrostra
import typhon
import matplotlib.pyplot as plt
import numpy as np
from copy import copy

phlev = typhon.math.nlogspace(1000e2, 20, 101)  # [Pa]
p = np.exp(0.5 * (np.log(phlev[1:]) + np.log(phlev[:-1])))  # [Pa]
T = typhon.physics.standard_atmosphere(p, coordinates='pressure')  # [K]
z = typhon.physics.pressure2height(p, T)  # [m]
latitude = 20  # [degrees]

# use RCEMIP ozone profile for the initial ozone
initial_o3 = 3.6478 * (p*0.01)**0.83209 * np.exp(-(p*0.01) / 11.3515) * 1e-6

ozone = Simotrostra()  # initialise the ozone model

time_step = 1/(24*12)  # [days]
max_days = 10  # how many model days to iterate for
times = np.arange(0, max_days, time_step)

zeros = np.zeros((times.shape[0], p.shape[0]))
source, sink_ox, sink_nox = copy(zeros), copy(zeros), copy(zeros)
sink_hox = copy(zeros)
o3_profile = np.zeros((times.shape[0]+1, p.shape[0]))
o3_profile[0] = initial_o3

i = 0
for time in times:
    zenith = np.rad2deg(np.arccos(
        np.cos(np.deg2rad(latitude)) * np.cos(2 * np.pi * time)))
    source[i], sink_ox[i], sink_nox[i], sink_hox[i] = ozone.tendencies(
        z, p, phlev, T, o3_profile[i], zenith)  # calculate the ozone tendencies
    o3_profile[i+1] = o3_profile[i] + (
           source[i] - sink_ox[i] - sink_nox[i] - sink_hox[i])*time_step
    i += 1

# Plot ozone profile at the end of the run and compare to the starting profile
plt.figure()
plt.plot(o3_profile[-1]*10**6, z*10**-3, label='end')
plt.plot(initial_o3*10**6, z*10**-3, label='start')
plt.ylim(np.min(z*10**-3), np.max(z*10**-3))
plt.ylabel('Height [km]')
plt.xlabel('Ozone concentration [ppmv]')
plt.legend()
plt.show()

# Plot mean ozone tendencies over the time of the run
plt.figure()
ci = 0
for term, tendency in [('source', source), ('sink from Ox', sink_ox),
                       ('sink from NOx', sink_nox), ('sink from HOx', sink_hox)
                       ]:
    for i in range(24*12):
        if i == 0:
            label = term
        else:
            label = ''
        typhon.plots.profile_p_log(p, tendency[-(i+1), :]*10**6, label=label, color=f'C{ci}', alpha=0.3)
    ci += 1
    #plt.plot(np.mean(tendency, axis=0)*10**6, z*10**-3, label=term)
#plt.ylim(np.min(z*10**-3), np.max(z*10**-3))
#plt.ylabel('Height [km]')
plt.xlim(0, 20)
plt.ylim(1000e2, 10)
plt.xlabel('Mean ozone tendency [ppmv / day]')
plt.legend()
plt.show()
