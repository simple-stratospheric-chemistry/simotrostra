import os
import numpy as np
from netCDF4 import Dataset
import scipy.constants as spc
import typhon.constants as tyc
from scipy.interpolate import interp1d
from simotrostra.utils import *

__all__ = [
    'Simotrostra'
]


class Simotrostra:
    """
    Simple stratospheric ozone chemistry scheme of Edward Charlesworth.
    """
    def __init__(self):
        super().__init__()

        self._parameters = Dataset(
            os.path.join(os.path.dirname(__file__),
                         '../data/SiRaChA_ozone_parameters.nc'))
        self._nox_parameters = Dataset(
            os.path.join(os.path.dirname(__file__),
                         '../data/acefts_nox_trop_djf.nc'))
        self._sigma_hz_o2 = None
        self._col_o2_toa = None
        self._NOx = None
        self._HOx = None
        self.o3_col = None
        self.o2_col = None
        self._cosfac = None
        self.o_to_o3 = None

    def herzberg(self, z, p, T, o2, numlevels):
        """
        Herzberg band diatomic oxygen photolysis rate calculation.

        The Herzberg band photolysis rate contributes (with the Schumann-Runge 
        bands) to diatomic oxygen photolysis. Combined, these create the only
        known source of odd-oxygen (O and O3) in the stratosphere.
        """

        # diatomic oxygen cross-section, depends on pressure
        if self._sigma_hz_o2 is None:  # calculate this once only
            sigma0 = add_layer_dim(self._parameters['sigma0_hz_o2'], numlevels)
            dsigmadP = add_layer_dim(self._parameters['dsigmadP_hz_o2'],
                                     numlevels)
            self._sigma_hz_o2 = (sigma0 + p * dsigmadP)

        sigma_hz_o3 = add_layer_dim(self._parameters['sigma_hz_o3'], numlevels)
        hz_flux = add_layer_dim(self._parameters['hz_flux'], numlevels
                                )*self._cosfac

        tr_o2 = np.zeros_like(hz_flux)
        for i in range(0, tr_o2.shape[0]):  # for each wavelength band
            # first multiply the number of o2 molecules by reaction cross-
            # section and then sum for the overhead column (done in the
            # function overhead_molecules)
            extinction = 1/self._cosfac * (
                overhead_molecules(o2 * self._sigma_hz_o2[i], p, z, T)
                + self._col_o2_toa * self._sigma_hz_o2[i, -1])
            tr_o2[i] = np.exp(-extinction)

        tr_o3 = np.exp(-self.o3_col * sigma_hz_o3)
        j2_hz = np.sum(tr_o2 * tr_o3 * self._sigma_hz_o2 * hz_flux, axis=0)

        return j2_hz

    def schumann_runge(self, numlevels):

        sigma_srb_o3 = add_layer_dim(self._parameters['sigma_srb_o3'],
                                     numlevels)
        sigma_srb_o2 = add_layer_dim(self._parameters['sigma_srb_o2'],
                                     numlevels)
        flux_srb = add_layer_dim(self._parameters['solar_flux_srb'],
                                 numlevels)*self._cosfac
        interval_srb = add_layer_dim(self._parameters['interval_srb'],
                                     numlevels)
        weights_srb = np.repeat(np.expand_dims(
                add_layer_dim(self._parameters['weights_srb'], numlevels),
                axis=0), sigma_srb_o3.shape[0], 0)  # for the subintervals
        tr_srb_o3 = np.exp(-self.o3_col * sigma_srb_o3)
        tr_srb_o2 = weights_srb * np.exp(-sigma_srb_o2 * self.o2_col)

        # photons / s (quantum efficiency is 1)
        # sum over subintervals and wavelengths
        j2_srb = np.sum(flux_srb * interval_srb * tr_srb_o3 * np.sum(
            sigma_srb_o2 * tr_srb_o2, axis=1), axis=0)

        return j2_srb

    def calculate_j3(self, numlevels):

        sigma_j3_o3 = add_layer_dim(self._parameters['sigma_j3_o3'], numlevels)
        flux_j3 = add_layer_dim(self._parameters['flux_j3'], numlevels
                                )*self._cosfac
        o3_tr = np.exp(-self.o3_col * sigma_j3_o3)
        j3 = np.sum(o3_tr * flux_j3 * sigma_j3_o3, axis=0)

        return j3

    def calculate_o_to_o3(self, p, T, o2, j3):
        """Calculate the ratio of atomic oxygen to ozone assuming steady state.

        Parameters:
            p (ndarray/int/float): pressure [Pa]
            T (ndarray/int/float): temperature [K]
            o2 (ndarray/float): oxygen concentration [ppv]
            j3 (ndarray/float): reaction rate [m6 / molecule2 / s]
        Returns:
            ndarray/float: ratio of atomic oxygen to ozone
        """
        # number density of molecules [molecules / m3]
        molecule_density = density_of_molecules(p, T)
        o2_m3 = o2 * molecule_density  # O2 molecules / m3

        k2 = calculate_k2(T)  # rate coefficient for O2 + M + 0 --> O3 + M
        # O to O3 ratio -- Brausseur and Solomon page 279 eqn 5.41b
        o_to_o3 = j3 / (k2 * o2_m3 * molecule_density)

        return o_to_o3

    def update_properties(self, z, p, phlev, T, o3, o2, zenith):
        """
        Calculate and store the total column oxygen and ozone [molecules / m2],
        and also J3.

        Parameters:
            z (ndarray): altitude [m]
            p (ndarray): pressure levels [Pa]
            phlev (ndarray): half pressure levels [Pa]
            T (ndarray): temperature profile [K]
            o3 (ndarray): ozone concentration profile [ppv]
            o2 (ndarray): oxygen concentration profile [ppv]
            zenith (float): angle to the sun [degrees]
        """
        self._cosfac = np.cos(np.deg2rad(zenith))
        flip_cosfac = 1 / self._cosfac

        if self._col_o2_toa is None:  # calculate only once
            # over-model-top oxygen column [molecules / m2]
            self._col_o2_toa = (phlev[-1] * tyc.avogadro) / (
                    spc.g * o2[-1] * 0.032)

        # overhead column densities [molecules / m2]
        self.o2_col = flip_cosfac * (
                overhead_molecules(o2, p, z, T
                                   ) + self._col_o2_toa)
        self.o3_col = flip_cosfac * (overhead_molecules(o3, p, z, T))

        j3 = self.calculate_j3(numlevels=len(p))
        self.o_to_o3 = self.calculate_o_to_o3(p, T, o2, j3)

        return

    def source(self, z, p, T, o2):
        """Calculate ozone source term.

        Parameters:
            p (ndarray): pressure levels [Pa]
            o2 (ndarray): oxygen concentration profile [ppv]

        Returns:
            ndarray: ozone source term [ppv / day]
        """

        j2_hz = self.herzberg(z, p, T, o2, len(p))
        j2_srb = self.schumann_runge(len(p))
        j2 = j2_hz + j2_srb  # [s-1]

        return 2 * j2 * o2 * 3600*24

    def ox_loss(self, p, T):
        """Calculate ozone losses by the Ox family, due to the reaction of
        atomic oxygen with ozone.

        Parameters:
            p (ndarray): pressure levels [Pa]
            T (ndarray): temperature profile [K]

        Returns:
            ndarray: ozone loss due to oxygen chemistry
        """
        molecule_density = density_of_molecules(p, T)  # [molecules / m3]

        k3 = calculate_k3(T)
        sink_ox = 2 * k3 * self.o_to_o3 * molecule_density * 3600 * 24

        return sink_ox

    def nox_loss(self, z, p, T):
        """Calculate ozone losses by the Ox and NOx families.

        Parameters:
            z (ndarray): altitude [m]
            p (ndarray): pressure levels [Pa]
            T (ndarray): temperature profile [K]

        Returns:
            ndarray: ozone loss due to nitrogen chemistry
        """
        molecule_density = density_of_molecules(p, T)  # [molecules / m3]

        b3, b4 = nox_reaction_rates(T)

        if self._NOx is None:  # calculate only once
            self._NOx = interp1d(self._nox_parameters['z'][:]*1000,
                                 self._nox_parameters['NOx'][:],
                                 fill_value='extrapolate')(z)
            np.nan_to_num(self._NOx, copy=False)
            self._NOx.clip(min=0., out=self._NOx)

        no_to_no2 = b3 * self.o_to_o3 / b4  # unitless
        no_to_nox = (1 + 1/no_to_no2)**-1  # unitless

        no = no_to_nox * self._NOx
        no2 = (1-no_to_nox) * self._NOx

        sink_nox = (b4 * no + b3 * no2 * self.o_to_o3)*molecule_density*3600*24

        return sink_nox

    def hox_loss(self, p, T, o3):
        """Calculate ozone losses by the HOx family.

        Parameters:
            p (ndarray): pressure levels [Pa]
            T (ndarray): temperature profile [K]
            o3 (ndarray): ozone concentration profile [ppv]

        Returns:
            ndarray: ozone tendency [ppv / day]
        """
        m_m3 = density_of_molecules(p, T)  # molecules / m3
        o3_m3 = o3 * m_m3  # O3 molecules / m3

        # rate constants
        a5, a7, a6, a6b = hox_reaction_rates(T)

        # OH and HO2 concentrations [molecules / m3]
        if self._HOx is None:  # calculate only once
            self._HOx = get_hox_data(p)

        # O concentrations [molecules / m3]
        o_m3 = o3_m3 * self.o_to_o3

        # applicable also lower, but still neglecting reactions with NO and CO
        # ho2_oh = a5*o_m3 * a6*o3_m3 / ((a7*o_m3 + a6b*o3_m3) * a5*o_m3)
        # cancel o_m3 and o3_m3 from numerator and denominator
        ho2_oh = a5 * a6 / ((a7*self.o_to_o3 + a6b) * a5)

        # OH, HO2 and H concentrations [molecules / m3]
        oh_m3 = self._HOx / (1 + ho2_oh)
        ho2_m3 = ho2_oh * oh_m3

        # loss of ozone due to HOx [molecules / m3 / s]
        loss_hox = (a5 * oh_m3 * o_m3 + a7 * ho2_m3 * o_m3)
        loss_hox_lower_stratosphere = a6 * oh_m3 * o3_m3 + a6b * ho2_m3 * o3_m3

        return (loss_hox + loss_hox_lower_stratosphere) / m_m3 * 3600 * 24

    def tendencies(self, z, p, phlev, T, o3, zenith, o2=None):
        """Calculate ozone tendencies.

        Parameters:
            z (ndarray): height levels [m]
            p (ndarray): pressure levels [Pa]
            phlev (ndarray): pressure values at model half levels [Pa]
            T (ndarray): temperature profile [K]
            o3 (ndarray): ozone concentration profile [ppv]
            zenith (float): angle to the Sun [degrees]
                between 0 (midday) and 180 (midnight)
        Optional parameters:
            o2 (ndarray): oxygen concentration profile [ppv]

        Returns:
            ndarray: ozone source term [ppv / day]
            ndarray: ozone sink due to Ox chemistry [ppv / day]
            ndarray: ozone sink due to NOx chemistry [ppv / day]
            ndarray: ozone sink due to HOx chemistry [ppv / day]
        """
        if o2 is None:  # assume an atmosphere with 21% oxygen by volume
            o2 = 0.21*np.ones_like(p)

        if zenith <= 89.5:  # if daytime and remove dangerous edge cases
            self.update_properties(z, p, phlev, T, o3, o2, zenith)
            source = self.source(z, p, T, o2)
            sink_ox = self.ox_loss(p, T)
            sink_nox = self.nox_loss(z, p, T)
            sink_hox = self.hox_loss(p, T, o3)
            return source, sink_ox * o3 ** 2, sink_nox * o3, sink_hox

        # if night
        zeros = np.zeros_like(p)
        return zeros, zeros, zeros, zeros
