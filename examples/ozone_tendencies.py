from simotrostra import Simotrostra
import typhon
import matplotlib.pyplot as plt
import numpy as np

phlev = typhon.math.nlogspace(1000e2, 20, 101)  # [Pa]
p = np.exp(0.5 * (np.log(phlev[1:]) + np.log(phlev[:-1])))  # [Pa]
T = typhon.physics.standard_atmosphere(p, coordinates='pressure')  # [K]
z = typhon.physics.pressure2height(p, T)  # [m]
zenith = 45  # [degrees]

# use RCEMIP ozone profile for the initial ozone
initial_o3 = 3.6478 * (p*0.01)**0.83209 * np.exp(-(p*0.01) / 11.3515) * 1e-6

ozone = Simotrostra()  # initialise the ozone model
source, sink_ox, sink_nox, sink_hox = ozone.tendencies(
    z, p, phlev, T, initial_o3, zenith)  # calculate the ozone tendencies

plt.figure()
for term, tendency in [('source', source), ('sink from Ox', sink_ox),
                       ('sink from NOx', sink_nox), ('sink from HOx', sink_hox)
                       ]:
    plt.plot(tendency*10**6, z*10**-3, label=term)
plt.ylim(np.min(z*10**-3), np.max(z*10**-3))
plt.ylabel('Height [km]')
plt.xlabel('Ozone tendency [ppmv / day]')
plt.legend()
plt.show()
