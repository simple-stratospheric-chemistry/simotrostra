from distutils.core import setup
from setuptools import find_packages


setup(
    name='SiRaChA',
    packages=find_packages(),
    description='SImultaneous RAdiative and CHemical integration of the Atmosphere',
    python_requires='>=3.5',
    include_package_data=True,
    install_requires=[
        'matplotlib',
        'numpy',
        'netcdf4>=1.2.7',
        'scipy>=0.19.0',
        'typhon>=0.7.0',
    ],
)
